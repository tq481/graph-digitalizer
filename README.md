# Graph Reader script

## TLDR
Script which extracts data from graph images and returns data in digital form.

## How does it work?
Script is based on OpenCV, it uses difference in point(node) and line(connection) size to recreate graph.
Script will only work, when graph is faced from left to right, and the nodes are located in columns.

Example graph:

![Example graph](/graf2.png)

## Dependencies
To run script you need:
 - [OpenCV](https:https://opencv.org/)
 - Python 3

To see working example check out `example.py`.
Important variable is dConst. It helps to determine if two points are in sthe same column. If is given too big, multiple points in different columns can be identified as one column. If too small, many columns can be created for one column in reality.
 
### Usage
The script will return two arrays. The first one is node coordinats (given in columns) and the second one is connection between nodes. It can be used to further graph analisys.
