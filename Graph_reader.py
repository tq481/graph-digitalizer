import cv2
import numpy as np
from copy import copy
import imutils
from imutils import contours
import sys
import os

#Output <if needed to be passed further> are:
#col - coordinates of the nodes (given in columns)
#conn - connections between nodes (connections between coordinates

class GraphReader:
	def __init__(self, dConst = 25):
		self.dConst = dConst
	
	def loadPhoto(self, root, name):
		try:
			self.photo = cv2.imread(os.path.join(root, name))
			assert(self.photo.shape[0] != 0 and self.photo.shape[1] != 0)
		except:
			print("Could not read given photo")
			exit()
	
	def generate(self, dConst = 25):
		self.dConst = dConst
		self._getPoints()
		self._getColumns()
		self.connections_ = self._getConnections()
	
	def getNodes(self):
		return self._centres
	
	def getConnections(self):
		return self.connections_
		
	def _getPoints(self):
		photo = copy(self.photo)
		photo = cv2.GaussianBlur(photo, (5, 5), 0)
		hsv_frame = cv2.cvtColor(photo, cv2.COLOR_BGR2HSV)
		lower = np.array((0,0,0), dtype = "uint8")
		upper = np.array((16,16,16), dtype = "uint8")
		mask = cv2.inRange(hsv_frame, lower, upper)
		thresh = cv2.erode(mask, None, iterations=1)
		thresh = cv2.dilate(thresh, None, iterations=1)
		thresh = cv2.threshold(thresh,200,255,cv2.THRESH_BINARY)[1]

		cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
								cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		
		centresAndRadius = ([],0)
		if(len(cnts)>0):
			cnts = contours.sort_contours(cnts)[0]
			self._centres, self._radius = self._pointsCentres(cnts)##########
	
	def _pointsCentres(self, cnts):
		radius = 0
		tab = []

		for w in range(len(cnts)):
			x = [cnts[w][0][0][0],cnts[w][0][0][0]]
			y = [cnts[w][0][0][1],cnts[w][0][0][1]]

			for e in range(len(cnts[w])):
				if(cnts[w][e][0][0] > x[0]):
					x[0] = cnts[w][e][0][0]
				if(cnts[w][e][0][0] < x[1]):
					x[1] = cnts[w][e][0][0]
				if(cnts[w][e][0][1] > y[0]):
					y[0] = cnts[w][e][0][1]
				if(cnts[w][e][0][1] < y[1]):
					y[1] = cnts[w][e][0][1]
			tab.append([int((x[0]+x[1])/2),int((y[0]+y[1])/2)])
			radius += (abs(x[0]-x[1]) + abs(y[0]-y[1]))/2
		return tab, int(radius/len(cnts))

	def _getColumns(self):
		self._columns = []
		for point in sorted(self._centres, key = lambda x: x[0]):
			isInCol = False

			for w in self._columns:
				for q in w:
					if abs(point[0] - q[0]) < self.dConst:
						isInCol = True
						w.append(point)
						break
			if(not isInCol):
				self._columns.append([point])

	def _getConnections(self):
		coorelation = []

		photo = copy(self.photo)
		hsv_frame = cv2.cvtColor(photo, cv2.COLOR_BGR2HSV)
		lower = np.array((0,0,0), dtype = "uint8")
		upper = np.array((16,16,16), dtype = "uint8")
		mask = cv2.inRange(hsv_frame, lower, upper)
		mask = cv2.threshold(mask,200,255,cv2.THRESH_BINARY)[1]

		#cv2.imshow("Debug",mask)
		#cv2.waitKey(0)

		for l in range(len(self._columns)-1):
			for w in self._columns[l]:
				for n in self._columns[l+1]:

					first = (int(w[0]),int(w[1]))
					last = (int(n[0]),int(n[1]))

					frameBoolean = np.zeros((len(self.photo),len(self.photo[0]),3),dtype = "uint8")

					frameBoolean = cv2.line(copy(frameBoolean),first,last,(255,255,255),self._radius)
					thresh = cv2.threshold(copy(frameBoolean),200,255,cv2.THRESH_BINARY)[1]
					thresh = cv2.bitwise_and(thresh,thresh,mask=mask)
					thresh = cv2.cvtColor(thresh, cv2.COLOR_BGR2GRAY)

					cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
											cv2.CHAIN_APPROX_SIMPLE)
					cnts = imutils.grab_contours(cnts)
					if(len(cnts)==1):
						coorelation.append([w, n])

		return coorelation
